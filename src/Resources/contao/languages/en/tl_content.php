<?php

/**
 * 361GRAD Element Teasertiles
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements'] = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_teasertiles'] = ['Teasertiles', 'Teasertiles.'];

$GLOBALS['TL_LANG']['tl_content']['firsttile_legend'] = 'First Tile (Settings)';
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileTitle'] = ['Tile Title', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileImage'] = ['Tile Image', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileImageSize'] = ['Image Size', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileImageAlt'] = ['Image Alt', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileLink'] = ['Tile Link', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileLinkText'] = ['Link Text', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileTarget'] = ['Link Target', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileLinkTitle'] = ['Link Titel', ''];

$GLOBALS['TL_LANG']['tl_content']['secondtile_legend'] = 'Second Tile (Settings)';
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileTitle'] = ['Tile Title', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileImage'] = ['Tile Image', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileImageSize'] = ['Image Size', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileImageAlt'] = ['Image Alt', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileLink'] = ['Tile Link', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileLinkText'] = ['Link Text', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileTarget'] = ['Link Target', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileLinkTitle'] = ['Link Titel', ''];

$GLOBALS['TL_LANG']['tl_content']['thirdtile_legend'] = 'Third Tile (Settings)';
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileTitle'] = ['Tile Title', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileImage'] = ['Tile Image', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileImageSize'] = ['Image Size', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileImageAlt'] = ['Image Alt', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileLink'] = ['Tile Link', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileLinkText'] = ['Link Text', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileTarget'] = ['Link Target', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileLinkTitle'] = ['Link Titel', ''];

$GLOBALS['TL_LANG']['tl_content']['margin_legend'] = 'Margin Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'] = ['Margin Top', 'Here you can add Margin to the top edge of the element (numbers only)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'] = ['Margin Bottom', 'Here you can add Margin to the bottom edge of the element (numbers only)'];
