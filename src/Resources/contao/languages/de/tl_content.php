<?php

/**
 * 361GRAD Element Teasertiles
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']    = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_teasertiles'] = ['Teaserkacheln', 'Kacheln Teaser.'];

$GLOBALS['TL_LANG']['tl_content']['firsttile_legend']  = 'Erste Kachel';
$GLOBALS['TL_LANG']['tl_content']['secondtile_legend'] = 'Zweite Kachel';
$GLOBALS['TL_LANG']['tl_content']['thirdtile_legend']  = 'Dritte Kachel';

$GLOBALS['TL_LANG']['tl_content']['dse_firstTileTitle']     = ['Titel', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileImage']     = ['Bild', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileImageSize'] = ['Bildgröße', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileImageAlt']  = ['Bild Alt-Tag', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileLink']      = ['Link Ziel', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileLinkText']  = ['Link Text', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileTarget']    = ['Link im neuen Fenster/Tab öffnen?', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_firstTileLinkTitle'] = ['Link Titel', ''];

$GLOBALS['TL_LANG']['tl_content']['dse_secondTileTitle']     = ['Titel', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileImage']     = ['Bild', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileImageSize'] = ['Bildgröße', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileImageAlt']  = ['Bild Alt-Tag', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileLink']      = ['Link Ziel', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileLinkText']  = ['Link Text', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileTarget']    = ['Link im neuen Fenster/Tab öffnen?', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_secondTileLinkTitle'] = ['Link Titel', ''];

$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileTitle']     = ['Titel', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileImage']     = ['Bild', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileImageSize'] = ['Bildgröße', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileImageAlt']  = ['Bild Alt-Tag', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileLink']      = ['Link Ziel', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileLinkText']  = ['Link Text', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileTarget']    = ['Link im neuen Fenster/Tab öffnen?', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileLinkTitle'] = ['Link Titel', ''];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']    = 'Randeinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']    = ['Rand oben', 'Hier können Sie Margin zum oberen Rand des Elements hinzufügen (nur nummern)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'] = ['Rand unten', 'Hier können Sie dem unteren Rand des Elements Rand hinzufügen (nur nummern)'];
