<?php

/**
 * 361GRAD Element Teasertiles
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_teasertiles'] =
    '{type_legend},type,headline;' .
    '{margin_legend},dse_marginTop,dse_marginBottom;' .
    '{firsttile_legend},dse_firstTileTitle,dse_firstTileImage,dse_firstTileImageSize,dse_firstTileImageAlt,dse_firstTileLink,dse_firstTileTarget,dse_firstTileLinkTitle;' .
    '{secondtile_legend},dse_secondTileTitle,dse_secondTileImage,dse_secondTileImageSize,dse_secondTileImageAlt,dse_secondTileLink,dse_secondTileTarget,dse_secondTileLinkTitle;' .
    '{thirdtile_legend},dse_thirdTileTitle,dse_thirdTileImage,dse_thirdTileImageSize,dse_thirdTileImageAlt,dse_thirdTileLink,dse_thirdTileTarget,dse_thirdTileLinkTitle;' .
    '{protected_legend:hide},protected;' .
    '{expert_legend:hide},guests,cssID;' .
    '{invisible_legend:hide},invisible,start,stop';


$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileTitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileTitle'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50',
        'preserveTags' => true,
        'decodeEntities' => true
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileImage'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileImage'],
    'inputType' => 'fileTree',
    'eval'      => [
        'fieldType'  => 'radio',
        'filesOnly'  => true,
        'extensions' => Config::get('validImageTypes'),
        'tl_class'   => 'clr'
    ],
    'sql'       => 'binary(16) NULL'
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileImageSize'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['size'],
    'inputType' => 'imageSize',
    'options'   => System::getContainer()->get('contao.image.image_sizes')->getAllOptions(),
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval'      => [
        'rgxp'               => 'digit',
        'includeBlankOption' => true,
        'tl_class'           => 'w50',
    ],
    'sql'       => "varchar(64) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileImageAlt'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileImageAlt'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileLink'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileLink'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'clr w50 wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileTarget'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileTarget'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50 m12',
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstTileLinkTitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstTileLinkTitle'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "mediumtext NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileTitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileTitle'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50',
        'preserveTags' => true,
        'decodeEntities' => true
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileImage'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileImage'],
    'inputType' => 'fileTree',
    'eval'      => [
        'fieldType'  => 'radio',
        'filesOnly'  => true,
        'extensions' => Config::get('validImageTypes'),
        'tl_class'   => 'clr'
    ],
    'sql'       => 'binary(16) NULL'
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileImageSize'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['size'],
    'inputType' => 'imageSize',
    'options'   => System::getContainer()->get('contao.image.image_sizes')->getAllOptions(),
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval'      => [
        'rgxp'               => 'digit',
        'includeBlankOption' => true,
        'tl_class'           => 'w50',
    ],
    'sql'       => "varchar(64) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileImageAlt'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileImageAlt'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileLink'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileLink'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'clr w50 wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileTarget'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileTarget'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50 m12',
    ],
    'sql'       => "mediumtext NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondTileLinkTitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondTileLinkTitle'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "mediumtext NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileTitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileTitle'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50',
        'preserveTags' => true,
        'decodeEntities' => true
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileImage'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileImage'],
    'inputType' => 'fileTree',
    'eval'      => [
        'fieldType'  => 'radio',
        'filesOnly'  => true,
        'extensions' => Config::get('validImageTypes'),
        'tl_class'   => 'clr'
    ],
    'sql'       => 'binary(16) NULL'
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileImageSize'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['size'],
    'inputType' => 'imageSize',
    'options'   => System::getContainer()->get('contao.image.image_sizes')->getAllOptions(),
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval'      => [
        'rgxp'               => 'digit',
        'includeBlankOption' => true,
        'tl_class'           => 'w50',
    ],
    'sql'       => "varchar(64) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileImageAlt'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileImageAlt'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileLink'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileLink'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'clr w50 wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileTarget'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileTarget'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50 m12',
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_thirdTileLinkTitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_thirdTileLinkTitle'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "mediumtext NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
